import React, { useState, useEffect } from 'react';
import axios from 'axios';

function AudioPlayer() {
    const [audioUrl, setAudioUrl] = useState('');

    useEffect(() => {
        fetchLatestAudio();
    }, []);

    const fetchLatestAudio = () => {
        axios.get('http://localhost:3001/latest')
            .then((response) => {
                setAudioUrl(response.data.audioUrl);
            })
            .catch((error) => {
                console.error('Error fetching audio:', error);
            });
    };

    return (
        <div>
            <audio controls>
                <source src={audioUrl} type="audio/wav" />
            </audio>
        </div>
    );
}

export default AudioPlayer;