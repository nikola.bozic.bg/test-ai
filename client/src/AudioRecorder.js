import React, { useState, useRef } from 'react';
import './AudioRecorder.css';
import {Audio, Radio} from "react-loader-spinner";
import {RiseLoader} from "react-spinners";

function App() {
    const [recording, setRecording] = useState(false);

    const audioRef = useRef(null);
    const audioStream = useRef(null);
    const [audioBlob, setAudioBlob] = useState(null);

    const [loading, setLoading] = useState(false);

    const startRecording = () => {
        console.log("recored started")
        navigator.mediaDevices.getUserMedia({ audio: true })
            .then(stream => {
                audioStream.current = stream;
                const mediaRecorder = new MediaRecorder(stream);
                const audioChunks = [];

                mediaRecorder.ondataavailable = event => {
                    if (event.data.size > 0) {
                        audioChunks.push(event.data);
                    }
                };

                mediaRecorder.onstop = () => {
                    const audioBlob = new Blob(audioChunks, { type: 'audio/wav' });
                    setAudioBlob(audioBlob);
                    uploadAudio(audioBlob); // Automatically upload and play after recording
                };

                mediaRecorder.start();
                setRecording(true);

            });
    };

    const stopRecording = () => {
        if (audioStream.current) {
            audioStream.current.getTracks().forEach(track => track.stop());
        }
        setRecording(false);
        setLoading(true)
    };

    const uploadAudio = (audioBlob) => {
        const formData = new FormData();
        formData.append('audio', audioBlob, 'recorded.mp3');

        fetch('http://3.127.32.199:3001/upload', {
            method: 'POST',
            body: formData,
        })
            .then(response => response.json())
            .then(data => {
                fetch(`http://3.127.32.199:3001/download/${data.filename}`)
                    .then(response => response.blob())
                    .then(blob => {
                        const audioURL = URL.createObjectURL(blob);
                        audioRef.current.src = audioURL;
                        setLoading(false)
                        audioRef.current.play();
                    });
            });
    };

    return (
        <div className="app-container">
            <div className="center-content">
                <button className="start-button" onClick={recording ? stopRecording : startRecording}>
                    {recording ? 'Finish question' : 'Tell me how can I help you'}
                </button>
                {loading && (
                    <RiseLoader
                        color="#36d7b7"
                        loading={loading}
                        margin={1}
                        size={15}
                    />
                )}
            </div>
            <audio controls ref={audioRef} style={{ display: 'none' }}></audio>
        </div>
    );
}

export default App;
