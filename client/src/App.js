import React from 'react';
import './App.css';
import AudioRecorder from './AudioRecorder'; // Import the AudioRecorder component

function App() {
  return (
      <div >
        <main>
          <AudioRecorder /> {/* Include the AudioRecorder component */}
        </main>
      </div>
  );
}

export default App;
