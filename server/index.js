const express = require('express');
const multer = require('multer');
const path = require('path');
const cors = require('cors');
const fs = require('fs');
const axios = require('axios');
const app = express();
const port = 3001;
const FormData = require("form-data")
const { Configuration, OpenAIApi } = require("openai");
var gtts = require('node-gtts')('en');
const AWS = require('aws-sdk')
const Fs = require('fs')



app.use(cors());


const storage = multer.diskStorage({
    destination: path.join(__dirname),
    filename: function (req, file, cb) {
        cb(null, 'recorded.wav');
    }
});


const upload = multer({ storage });
app.use(express.static(path.join(__dirname)));

app.post('/upload', upload.single('audio'),  async (req, res) => {

    let data = new FormData();
    data.append("file", fs.createReadStream(req.file.path));
    data.append("model", "whisper-1");
    data.append("language", "en");

    let config = {
        method: "post",
        maxBodyLength: Infinity,
        url: "https://api.openai.com/v1/audio/transcriptions",
        headers: {
            Authorization: `Bearer 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`,
            "Content-Type": "multipart/form-data",
            ...data.getHeaders(),
        },
        data: data,
    };

    const response = await axios.request(config);
    console.log("question", response.data.text)



    const configuration = new Configuration({
        apiKey: "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",
    });

    const openai = new OpenAIApi(configuration);

    const messages = [];
    messages.push({role: "system", "content": "You are a virtual psychiatrist"})
    messages.push({role: "user", content: response.data.text});

    const completion = await openai.createChatCompletion({
        model: "gpt-3.5-turbo",
        messages: messages,
    })

    const completion_text = completion.data.choices[0].message.content;
    console.log("answer",completion_text);

    const filepath = path.join(__dirname, 'answer.mp3');

    const Polly = new AWS.Polly({
        signatureVersion: 'v4',
        region: 'eu-central-1',
        accessKeyId: 'AKIA3D2ICE3QIF7MTF5E',
        secretAccessKey: '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'
    })

    let params = {
        'Text': completion_text,
        'OutputFormat': 'mp3',
        'VoiceId': 'Kendra'
    }

    Polly.synthesizeSpeech(params, (err, data) => {
        if (err) {
            console.log(err.code)
        } else if (data) {
            if (data.AudioStream instanceof Buffer) {
                Fs.writeFile(filepath, data.AudioStream, function(err) {
                    if (err) {
                        return console.log(err)
                    }
                    res.header('Access-Control-Allow-Origin', '*');
                    res.header('Access-Control-Allow-Methods', 'GET');
                    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
                    res.json({ filename: 'answer.mp3' });
                    console.log("The file was saved!")
                })
            }
        }
    })

    // gtts.save(filepath, completion_text, function () {
    //     console.log('save done');
    //     res.json({ filename: 'answer.wav' });
    // })

     // res.json({ filename: 'answer.wav' });
});


app.get('/download/:filename', (req, res) => {
    const filename = req.params.filename;
    console.log("download " + filename)
    const filePath = path.join(__dirname, filename);
    res.sendFile(filePath);
});

app.get('/health', (req, res) => {
    res.status(200).send('Server is healthy');
});



app.listen(port, () => {
    console.log(`Server listening at http://localhost:${port}`);
});